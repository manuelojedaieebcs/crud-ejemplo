<?php
// phpcs:disable
namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        User::truncate();
        $users = factory(User::class, 30)->create();
        $response = $this->json('GET', '/');
        $response->assertJsonStructure([
            'data' => [
                '*' =>  ['id', 'name', 'email', 'email_verified_at', 'password', 'remember_token','created_at', 'updated_at']
            ]
        ])
            ->assertStatus(200);
    }
}
