@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 my-5">
              @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
              @endif
              <form action="{{ route('users.update', $user) }}" method="post">
                <div class="form-group">
                  <label for="">
                    Name
                  </label>
                  <input
                    type="text"
                    name="name"
                    class="form-control"
                    value="{{ old('name', $user->name) }}"
                    required
                  >
                </div>
                <div class="form-group">
                  <label for="">
                    Email
                  </label>
                  <input
                    type="email"
                    name="email"
                    class="form-control"
                    value="{{ old('email', $user->email) }}"
                    required
                  >
                </div>
                <div class="form-group">
                  <label for="">
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    class="form-control"
                    value=""
                  >
                </div>
                <div class="form-group">
                  @csrf
                  @method('PUT')
                  <input type="submit" value="Send" class="btn btn-sm btn-primary">
                  <a href="{{ route('users.index') }}" class="btn btn-secondary btn-sm">
                    Abort
                  </a>
                </div>
              </form>
            </div>
        </div>
    </div>
@endsection