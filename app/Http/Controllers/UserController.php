<?php
// phpcs:disable
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    public function index()
    {
        $users = User::latest()->paginate();
        return view('welcome')
            ->with('users', $users);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(UserRequest $request)
    {
        // dd($request->all());
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        return \redirect('/');
    }

    public function edit(User $user)
    {
        return view('users.edit')
            ->with('user', $user);
    }

    public function update(UserRequest $request, User $user)
    {
        $user->name = $request->name;
        $user->email = $request->email;
        
        if ($request->password) {
            $user->password = $request->password;
        }
        
        $user->save();
        return \redirect('/');
    }

    public function destroy(User $user)
    {
        $user->delete();
        return \redirect('/');
    }
}
